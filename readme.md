#First experiment
In our first experiment only one signal from the headset was used: we noticed that muscular movements were the strongest so we used the eyebrow raise value as a main input. So, just to show a bit of magic to our audience we came up with a  Processing sketch that lets the user trash pieces of paper when eyebrow were raised (have a look at the video :) )

NOTE: The sensors' values were passed to Processing via osc using MindYourOSC.
The Processing sketch is using the osc library by Andreas Schlegel and the box2d port by Daniel Shiffman.

[screenshot / video ?]


#Going further
The next step was to get the raw signals from the headset. We couldn't find a Processing library so we used a java library by Samuel Halliday [https://github.com/fommil/emokit-java] who ported the open source Emokit library originally written in C developed by several brave people properly credited on the original repository: [https://github.com/openyou/emokit].

From this library we could get the strength and the quality of each electrode's signal. 


#Visualizing the output
Once we had the library working with Processing, we needed to visualize four variables for each sensor:

- The position on the scalp 
- The quality of the signal
functions alraedy present in the EPOC control panel

- The strength of the signal 
- The change of strength over time

Working with Giulia Marzin[http://giuliamarzin.tumblr.com] we created a polar graph that shows the strength of the signal and a color indicator for its quality.

[immagine ]


#Using EEG data for a poster design
Having our sketch running in Processing we could easily export the data we were visualizing and use it as an input for other applications (such as visualizations, physical computing etc.)

As a way of making our work public within the lab we created some posters using the Processing output in Illustrator. In the process we stumbled upon an interesting Moiré effect that gave the sensors graphs a watercolor-like effect. 

[immagine poster]
[foto poster]


#Interfacing with Arduino
Using the data of the neural helmet with an Arduino was our next step: in this test experiment we mapped the first six signals values to a LED.

It would be interesting to add these LEDs on the helmet itself.


[video / screenshot ]

#Integrating MindYourOSC and raw sensor data 



#Machine Learning

Rows represent the observations and columns contain the flattened time values for each channel.