/**
 * ProcessingEpocOsc1
 * by Joshua Madara, hyperRitual.com
 * demonstrates Processing + Emotiv EPOC via OSC
 * uses EPOC's Cognitiv Left and Right to move a circle
 * left or right
 */

import oscP5.*;
import netP5.*;
import shiffman.box2d.*;
import org.jbox2d.collision.shapes.*;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.*;




ParticleSystem ps;
ParticleSystem ps2;
ParticleSystem ps3;

public float cogLeft = 0;
public float cogRight = 0;
int circleX = 240;

boolean addNew = false;
OscP5 oscP5;

// A reference to our box2d world
Box2DProcessing box2d;

// A list we'll use to track fixed objects
ArrayList<Boundary> boundaries;
// A list for all of our rectangles
ArrayList<Box> boxes;
PImage scarta;
void setup() {
  size(800, 600, OPENGL);


  //start oscP5, listening for incoming messages on port 7400
  //make sure this matches the port in Mind Your OSCs
  oscP5 = new OscP5(this, 7400);

  // Initialize box2d physics and create the world
  box2d = new Box2DProcessing(this);
  box2d.createWorld();
  // We are setting a custom gravity
  box2d.setGravity(0, -20);

  noStroke();
  // Create ArrayLists  
  boxes = new ArrayList<Box>();
  boundaries = new ArrayList<Boundary>();

  // Add a bunch of fixed boundaries
  boundaries.add(new Boundary(width/4, height-5, width, 10));
  //  boundaries.add(new Boundary(3*width/4, height-5, width/2-100, 10));
  boundaries.add(new Boundary(width-5, height/2, 10, height));
  boundaries.add(new Boundary(5, height/2, 10, height));

  scarta = loadImage("scarta.png");
  PImage img = loadImage("texture.png");
  ps = new ParticleSystem(0, new PVector(width  -50, height+40), img);
  ps2 = new ParticleSystem(0, new PVector(width  -70, height), img);
  ps3 = new ParticleSystem(0, new PVector(width  -90, height+40), img);
}

void draw() {
  background(0);

  // We must always step through time!
  box2d.step();

  // When the mouse is clicked, add a new Box object
  if (random(1)>.9) {
    Box p = new Box(random(width-300), -50);
    boxes.add(p);
    addNew  = false;
  }

  for (Box b : boxes) {
    Vec2 wind = new Vec2(cogLeft*100, 0);
    b.applyForce(wind);
  }


  // Display all the boundaries
  for (Boundary wall : boundaries) {
    wall.display();
  }

  // Display all the boxes
  for (Box b : boxes) {
    b.display();
  }

  // Boxes that leave the screen, we delete them
  // (note they have to be deleted from both the box2d world and our list
  for (int i = boxes.size ()-1; i >= 0; i--) {
    Box b = boxes.get(i);
    if (b.done()) {
      boxes.remove(i);
    }
  }

  fill(0);

  //
  // Calculate a "wind" force based on mouse horizontal position
blendMode(ADD);

  ps.run();
  for (int i = 0; i < 2; i++) {
    ps.addParticle();
  }
  
   ps2.run();
  for (int i = 0; i < 2; i++) {
    ps2.addParticle();
  }
  
   
   ps3.run();
  for (int i = 0; i < 2; i++) {
    ps3.addParticle();
  }
  
}




void oscEvent(OscMessage theOscMessage) {
  // check if theOscMessage has an address pattern we are looking for
  if (theOscMessage.checkAddrPattern("/EXP/EYEBROW") == true) {
    // parse theOscMessage and extract the values from the OSC message arguments
    cogLeft = theOscMessage.get(0).floatValue();
    if(cogLeft >0) println(cogLeft);
  } 

  if (theOscMessage.checkAddrPattern("/EXP/BLINK") == true) {
    addNew =true;
    //    println("going");
  }
}

