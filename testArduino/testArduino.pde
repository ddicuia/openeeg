import com.github.fommil.emokit.*;
import oscP5.*;
import netP5.*;
import java.util.Map;
import java.util.Map.Entry;
import controlP5.*;


import processing.serial.*;

Serial myPort;  // Create object from Serial class
int val;        // Data received from the serial port
int oldMillis = 0;





BrainView brainView;
EpocOSCData emotivOSC;

OscP5 oscP5;

boolean debug = false;

boolean stopDrawing = false;
boolean message = false;


color to = color(0, 0, 255);
color from = color(204, 204, 204);




ControlP5 controlP5;

Experiment experiment;

int sensors = 14;
int timeslices = 600;
int classes = 2;

void setup() {


	size(1280, 800, OPENGL);
	// ,  "processing.core.PGraphicsRetina2D"); // for retina displays
	// size(displayWidth, displayHeight, OPENGL);
	noStroke();
	background(220);

	textFont(loadFont("SimplonBPMono-Medium-12.vlw"), 12);


	String portName = "/dev/tty.usbmodem1421";
	myPort = new Serial(this, portName, 9600);


	
	experiment = new Experiment(classes, sensors, timeslices);
	experiment.newObservation();

	float angle = (PI * 2) / experiment.nTimeSlices;
	SensorData sensorData = new SensorData();
	
	brainView = new BrainView(sensorData, experiment);
	controlP5 = new ControlP5(this);
	
	Gui gui = new Gui(10,10);


	if(!debug) {

		try {
			Emotiv emotiv = new Emotiv();
			emotiv.addEmotivListener(sensorData);
			emotiv.start();

			/* OSC setup */

			emotivOSC = new EpocOSCData();

  		//start oscP5, listening for incoming messages on port 7400
  		//make sure this matches the port in Mind Your OSCs
  		oscP5 = new OscP5(this, 7400);
  	} 
  	catch(IOException e) {
  		println(e);
  	}
  }



  background(0);




}

void keyPressed() {
	if(key == 's') {
		stopDrawing = !stopDrawing;
	} else {
		brainView.clear();
	}
}

void draw() {

	if(stopDrawing) return;
	
	//TODO change this to be augmented on user input
	if (frameCount % experiment.nTimeSlices == 0 ) {
		brainView.clear();
		experiment.newObservation();
	}


	background(220);
	pushMatrix();
	translate((width-brainView.dimX)*.5, (height-brainView.dimY)*.5);
	experiment.update();
	brainView.draw();
	//emotivOSC.draw();
	popMatrix();
	drawCorrelationGraph();


	String values = "";
	for (int i = 0; i < experiment.nSensors; ++i) {
		println(experiment.currentObservation[i][experiment.timeCounter]);
		String a = experiment.currentObservation[i][experiment.timeCounter] > 0.65 ? "1" : "0";
		values += a;
	}
	myPort.write(values);

}

/**
 * Simple Write. 
 * 
 * Check if the mouse is over a rectangle and writes the status to the serial port. 
 * This example works with the Wiring / Arduino program that follows below.
 */


 void drawCorrelationGraph() {


 	int graphHeight = 100;
 	int graphWidth = 400;
 	int col =  graphWidth / experiment.nSensors;

 	if(experiment.observations.size() >1 ) {


 		for (int i = 0; i < experiment.observations.size() -1; ++i) {

 			float [][] observation = experiment.observations.get(i);

 			float prevX=0;
 			float prevY =0;

			//for each column
			for (int k = 0; k < experiment.nSensors; ++k) {

				float total = 0;
				float average;
				for (int z = 0; z < observation[k].length; ++z) {
					total += observation[k][z];
				}


				average = total / observation[k].length;


				float x =  (k * col) ;
				float y = average * graphHeight;
				stroke(from);
				line(x, 0, x, graphHeight);
				if(k > 0)  {
					stroke(lerpColor( from, to, map(i, 0, experiment.observations.size()-1,  0,1) ) ); 
					line(prevX, prevY,x,y);
				}
				// ellipse(x,y,10,10);
				prevX = x;
				prevY = y;

			}

		}

	}
}


	// boolean sketchFullScreen() {
	// 	return true;
	// }
